#-------------------------------------------------
#
# Project created by QtCreator 2016-01-14T20:18:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SampleFInder
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++11

SOURCES += main.cpp\
        mainwindow.cpp \
    samplefinder.cpp \
    processcontroller.cpp

HEADERS  += mainwindow.h \
    samplefinder.h \
    processcontroller.h

FORMS    += mainwindow.ui
