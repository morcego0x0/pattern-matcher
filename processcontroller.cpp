#include "processcontroller.h"

#include "samplefinder.h"
#include "mainwindow.h"

ProcessController::ProcessController(QObject *parent)
    : QObject(parent),
      m_finder(new SampleFinder(this))
{

}

void ProcessController::processFromFile(const QString &fileName)
{
    m_finder->openFile(fileName);
}

void ProcessController::searchPattern(const QString &pattern)
{

    m_finder->countPattern(pattern);
}

void ProcessController::setView(MainWindow *view)
{
    m_view = view;

    connect(m_finder, &SampleFinder::matchFound, m_view, &MainWindow::matchFound);
    connect(m_finder, &SampleFinder::dataToProcess, m_view, &MainWindow::dataSise);
    connect(m_finder, &SampleFinder::processed, m_view, &MainWindow::dataProcessed);
}

