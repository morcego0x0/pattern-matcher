#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class ProcessController;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_startProcessing_released();

    void on_openFile_released();

public slots:
    void dataSise(int size);
    void dataProcessed(int size);
    void matchFound(int found);

private:
    Ui::MainWindow *ui;
    ProcessController* m_controller;
    int m_matchCount;
};

#endif // MAINWINDOW_H
