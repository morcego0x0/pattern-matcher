#include "samplefinder.h"
#include <QFile>


SampleFinder::SampleFinder(QObject *parent)
    : QObject(parent)
{

}

void SampleFinder::openFile(const QString &filePath)
{
    m_file.setFileName(filePath);
    emit dataToProcess(m_file.size());
}

void SampleFinder::countPattern(const QString &pattern)
{
    m_file.open(QIODevice::ReadOnly);
    calcTable(pattern.toStdString());

    const qint64 max_size = 1024 * 1024;
    char data[max_size];
    int found = 0;
    int processed_data = 0;
    while(!m_file.atEnd()) {
        const qint64 read = m_file.read(data, max_size);
        processed_data += read;
        std::string str(data, data + read);

        found += countMatchInBuf(pattern.toStdString(), str);
        emit matchFound(found);
        emit processed(processed_data);
    }

    m_file.close();
}

int SampleFinder::countMatchInBuf(const std::string &pattern, const std::string &data)
{
    int i = 0;
    bool found = true;
    int found_num = 0;

    while (i + pattern.size() <= data.size()) {
        found = true;
        for (int k = pattern.size() - 1; k >= 0; --k) {
            if (data[i + k] != pattern[k]) {
                found = false; break;
            }
        }
        if (found) {
            found_num++;
        }

        i += shiftTo(data[i + (pattern.size() - 1)], pattern.size());
    }
    return found_num;
}

void SampleFinder::calcTable(const std::string &pattern)
{
    m_table.clear();
    const int pt_size = pattern.size();
    for (int i = 0; i < pt_size - 1; ++i) {
        m_table[pattern[i]] = pt_size - i - 1;
    }
    m_table[pattern[pt_size - 1]] = pt_size;
}

int SampleFinder::shiftTo(char symbol, int max_val) const
{
    Table::const_iterator it = m_table.find(symbol/*data[i + last_index]*/);
    if (it != m_table.end()) {
        return it->second;
    }

    return max_val;
}
