#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "processcontroller.h"

#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_controller(new ProcessController(this)),
    m_matchCount(0)
{
    ui->setupUi(this);
    ui->progressBar->setMaximum(0);
    ui->progressBar->setMinimum(0);

    ui->found_count->setText(QString::number(0));

    m_controller->setView(this);

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_openFile_released()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "C://",
                                                    tr("Text (*.txt)"));
    ui->filePath->setText(fileName);
    m_controller->processFromFile(fileName);
}

void MainWindow::on_startProcessing_released()
{
    m_matchCount = 0;
    ui->found_count->setText(QString::number(0));
    ui->progressBar->setValue(0);
    QString pattern = ui->searchPattern->text();
    if (!pattern.isEmpty()) {
        m_controller->searchPattern(pattern);
    }
}

void MainWindow::dataSise(int size)
{
    ui->progressBar->setMaximum(size);
}

void MainWindow::dataProcessed(int size)
{
    ui->progressBar->setValue(size);
}

void MainWindow::matchFound(int found)
{
    ui->found_count->setText(QString::number(found));
}

