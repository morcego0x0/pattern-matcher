#ifndef SAMPLEFINDER_H
#define SAMPLEFINDER_H

#include <QObject>
#include <QByteArray>
#include <QFile>
#include <QString>

#include <unordered_map>

using Table = std::unordered_map<char, int>;

class SampleFinder : public QObject
{
    Q_OBJECT
public:
    explicit SampleFinder(QObject *parent = 0);

    /**
     * @brief openFile set the path to process the data
     *
     * @param filePath path to file on the filesystem
     */
    void openFile(const QString& filePath);

    /**
     * @brief countPattern The function which allows to count
     * certain pattern in the specified file
     *
     * @param pattern pattern to search and count
     */
    void countPattern(const QString &pattern);

signals:
    /**
     * @brief matchFound
     */
    void matchFound(int);

    /**
     * @brief dataToProcess
     * @param bytes
     */
    void dataToProcess(int bytes);

    /**
     * @brief processed
     * @param bytes
     */
    void processed(int bytes);

private:
    /**
     * @brief countMatchInBuf counts how match time the pattern exists in the certain buffer
     *
     * @param pattern pattern to search and count
     *
     * @param data data where the certain pattern should be found
     *
     * @return how mush time the pattern are exists in the ceratin buffer.
     */
    int countMatchInBuf(const std::string &pattern, const std::string& data);

    /**
     * @brief calcTable allow to calcualte shiftting table
     *
     * @param pattern the pattern which used to generate the table
     */
    void calcTable(const std::string& pattern);

    /**
     * @brief shiftTo alows to calculate the index for shifting within data buffer
     *
     * @param symbol last symbol
     *
     * @param max_val the value which should be returned in case of absence specific value
     * for the passed character.
     *
     * @return position for shiftting
     */
    int shiftTo(char symbol, int max_val) const;

    QFile m_file;
    Table m_table;
};

#endif // SAMPLEFINDER_H
