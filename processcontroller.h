#ifndef PROCESSCONTROLLER_H
#define PROCESSCONTROLLER_H

#include <QObject>
#include <QString>

class SampleFinder;
class MainWindow;

class ProcessController : public QObject
{
    Q_OBJECT
public:
    explicit ProcessController(QObject *parent = 0);

    void processFromFile(const QString& fileName);
    void searchPattern(const QString& pattern);
    void setView(MainWindow* view);
signals:

private:
    SampleFinder* m_finder;
    MainWindow* m_view;
};

#endif // PROCESSCONTROLLER_H
